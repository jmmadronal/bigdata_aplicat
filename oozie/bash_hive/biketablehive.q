# Accedir a la base de dades
USE CapitalBikeHive;

# Cream la taulal capitalbike
# En aquest taula es guarden els valor intermitjos
CREATE TABLE IF NOT EXISTS capitalbike (
  Duration int,
  Start_date timestamp, 
  End_date timestamp, 
  Start_station_number string, 
  Start_station string, 
  End_station_number string, 
  End_station string, 
  Bike_number string, 
  Member_type string
  )
  COMMENT 'Capital Bike Table'
  ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
  STORED AS TEXTFILE ;

# Es carreguen les dades del fitxer csv a la taula capitalbike
LOAD DATA INPATH '/user/cloudera/WorkspaceOozieShellHive/capitalbike_noheader.csv' INTO TABLE capitalbike;

# A partir de la taula capitalbike inserim valors dins la taula bike
INSERT overwrite TABLE bike
SELECT year(Start_date), weekofyear(Start_date), Bike_number, sum(Duration) 
FROM capitalbike 
GROUP BY Duration, year(Start_date), weekofyear(Start_date), Bike_number;

# Esborrar la taula capitalbike
DROP TABLE capitalbike;
