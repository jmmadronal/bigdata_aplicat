#!/bin/bash
# Aquest script permet baixar un fitxer i descomprimir-ho
wget $1 -O capitalbike.zip
# Opcionalment es pot pujar el fitxer zip
#hdfs dfs -put capitalbike.zip /user/cloudera/WorkspaceOozieShellHive

# Descomprimir el fitxer
unzip -c capitalbike.zip > capitalbike.csv

# Llevar les 4 primeres files que es corresponen amb les capçaleres
tail -n +4 capitalbike.csv > capitalbike_noheader.csv

# Pujar el fitxer al hdfs
hdfs dfs -put capitalbike_noheader.csv /user/cloudera/WorkspaceOozieShellHive
