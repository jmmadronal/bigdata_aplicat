/* Descarregam el fitxers */
sh wget https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/pig/Test.csv
/* Cream les carpetes */
cd hdfs:///
cd /user/cloudera
mkdir pig_analisis_opinions
cd pig_analisis_opinions
/* Copiam el fitxers*/
copyFromLocal Test.csv Test.csv
/* Carregar les taules a memoria */
register /usr/lib/pig/piggybank.jar
analisisopinions = LOAD 'Test.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE') AS (text:chararray, label:int);
/* Afegir una columne amb un n. aleatori del 0..15*/
analisisopinionsclass = FOREACH analisisopinions GENERATE *, ROUND(RANDOM()*15) AS id;
/* Guadar el resultat */
STORE analisisopinionsclass INTO 'analisisopinions' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE');
/* Copiar al directori local */
copyToLocal analisisopinions/part-m-00000 critiquescinematografiques.csv;
