/* Carregar les dades */
casualtyRates = load '$INPUT' using PigStorage(',') AS
(country:chararray, 
year:int, 
sex:chararray, 
age:chararray, 
casualties_no:int,
population:int,
casualties100k_pop:float,
country_year:chararray,
HDI_for_year:float,
gdp_for_year_dolar:long,
gdp_per_capita_dolar:int,
generation:chararray);
/*
Consultar
Nombre total de casualties per país 

Agrupar les taxes per país */
country_rates_casualties = GROUP casualtyRates By country;
/*
Per cada un dels països sumar el nombre total */
country_rates_casualties_no = FOREACH country_rates_casualties GENERATE group,SUM(casualtyRates.casualties_no) AS total;
/*
Ordenar de forma descendent els resultats */
country_rates_casualties_no = ORDER country_rates_casualties_no BY total DESC;
/*
Guardar els resultats */
STORE country_rates_casualties_no INTO '$OUTPUT' USING PigStorage (',');
