# AWS Glue amb json
En aquesta documentació explica com fer un Procés ETL utilitzant AWS Glue a partir d'un fitxer json

La documentació de referència utilitzada a estat el laboratori:
"Lab: Performing ETL on a Dataset by Using AWS Glue" de "AWS Academy Data Engineering"

Aquest laboratori s'ha provat amb un AWS Academy Learner Lab

## Accés al laboratori AWS Academy Learner Lab

1. Seleccionar **>Start Lab**
1. Quan **AWS** es mostri en verd ja podrem obrir la consola d'AWS.

## Preparar les carpetes i fitxers a AWS S3.
Per fer això abans s'haura de crear un bucket, crear dues carpetes una amb el fitxer json i l'altre per guardar les consultes amb **Amazon Athena**

1. Anar al servei AWS S3.
1. Seleccionar **Buckets**
1. Seleccionar **Create bucket**
1. Per **Bucket name** introduir  `data-bucket-12345` 
1. Deixar les opcions per defecte i pitjar el botó **Create bucket**
1. Seleccionar **Create folder**
1. Per **Folder name** introduir `athena`
1. Seleccionar **Create folder**
1. Tornar a seleccionar **Create folder**
1. Per **Folder name** introduir `meteorite`
1. Seleccionar **Create folder**
1. Seleccionar la carpeta **meteorite**
1. Seleccionar un json per pujar. 
    > Nota: Del següent enllaç https://github.com/jdorfman/awesome-json-datasets
s'ha seleccionat  [Earth Meteorite Landings](https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/glue_json/meteorites.json) el qual està pujat a un repositori de gitlab
1. Seleccionar **data-bucket-12345**.
1. Seleccionar **Upload**, **Add files**
1. Pujar l'arxiu **meteorites.json**

## Utilitzar AWS Glue crawler amb json
1. Anar al servei **AWS Glue**.
1. Seleccionar **Data Catalog->Databases->Tables**.
1. Seleccionar **Add tables using crawler**.
1. Per **Name** introduir `Meteorite`.
1. Seleccionar **Next** a la part inferior de la pàgina.
1. Seleccionar **Add a data source** i configurar el següent:
    * **Data source:** Seleccionar **S3**.
    * **Location of S3 data:** Seleccionar **In thisa account**.
    * **S3 path:** Introduir la seguent localització:
    ```
    s3://data-bucket-12345/meteorite
    ```
    * **Subsequent crawler runs:** seleccionar **Crawl all sub-folders**.
    * Seleccionar **Add an S3 data source**.
1. Seleccionar **Next**.
1. Per **Existing IAM role**, selecconar **LabRole**.
1. Per a la secció **Output configuration**, seleccionar **Add database**.
1. Per **Name**, introduir `meteoritedata`
1. Seleccionar **Create database**.
1. Torna a la pestanya del navegador que està oberta a la pàgina **Set output and scheduling** dins **AWS Glue console**.
1. Per **Target database**, selecciona **meteoritedata**
1. Dins la secció **Crawler schedule**, a **Frequency** seleccionar **On demand**.
1. Seleccionar **Next**.
1. Confirma la configuració i seleccionar **Create crawler**.
1. Executar el crawler.
    * Dins la pàgina **Crawlers**, seleccionar **meteorite** el crawler que has acabat de crear
    * Selecconar **Run**. I l'estat passa a *Running*

## Revisar les metadates creade amb AWS Glue.
1. Al panell de navegació seleccoinar **DAtabases**
1. seleccionar el link per la base de dades **meteoritedata** 
1. Dins la secció de **Tables**, seleccionar **Meteorite**

## Visualitzar les dades amb **Amazon Athena**.
1. Accedir a la consola Amazon Athena
    * Al panell de navegació, davall **Databases**, seleccionar **Tables**
    * Selecconar la taula **meteorite**
    * Seleccionar **Actions > View data**.
    * seleccionar **Proceed** del pop-up. 
1. Configurar S3 bucket per guardar els resultats de Amazon Athena després que s'ha obert la consola
    * Seleccionar la pestanya **Settings**
    * Seleccionar **Manage**.
    * A la dreta de **Location of query result**, seleccinar **Browse S3**.
    * Seleccionar el bucket i la carpeta **data-bucket-12345/athena**
    * Seleccionar **Save**
1. Previsualitzar la taula
    * Seleccionar la pestanya **Editor**
    * Dins el panell **Data** a l'esquerra ha d'aparèixer **AwsDataCatalog** per **Data source**
    * Per **Databases**, seleccionar **meteoritedata**
    * Dins la secció **Tables** seleccionar **meteorite**
    * Seleccionar els tres puntets i llavors seleccoinar **Preview Table**
1. Fer una sentència SQL que mostre el nom del meteorit i les coordenades:
    ```
    SELECT name, geolocation.coordinates[2],geolocation.coordinates[1] FROM "AwsDataCatalog"."meteoritedata"."meteorite" limit 10;
    ```





