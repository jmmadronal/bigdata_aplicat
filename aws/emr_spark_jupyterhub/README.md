# AWS EMR spark i jupyterhub
En aquesta documentació explica com poder deplegar i utilitzar un clúster EMR amb *spoark* i *jupyterhub*.
Al directori aws/emr_spark_jupyterhub hi ha una sèrie d'scripts, fitxer de configuració i exemples de notebooks,

La documentació de referència utilitzada a sigut:
https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-jupyterhub.html

Les utilitats que permeten crear el clúster emrSparkJupyterhub es troben al següent enllaç.
https://gitlab.com/jmmadronal/bigdata_aplicat/-/tree/main/aws/emr_spark_jupyterhub

Descripció de les utilitats:
 * **README.md**: Ajuda per la implantació del clúster
 * **emrSparkJupyterHub.sh**: Script principal a partir del qual es pot crear un clúster emr.
 * **install-my-jupyter-libraries.sh**: Intal·lacions de le llibreries necessàries per al node mestre i els esclaus
 * **MyJupyterConfig.json**: Fitxer de configuració per fer persistens els notebooks i guardar-los a S3.
 * **datos_cluster.csv**: Dades de prova utilitzades al notebook.
 * **M05_BDA_U2_P5_V08_Ejemplo 3-Clustering K-means.ipynb**: Notebook exemple.
 
Per implementar el clúster EMR amb l'*spark* i el *jupyterhub* es pot fer utilitzant un compte propi de AWS, en aquest cas s'ha de vigilar per no incórrer amb despeses elevades. El més indicat és utilitzar un **AWS Academy Learner Lab** per poder fer proves i disposar de 100$ de credit.

En el cas d'utilitzar un **AWS Academy Learner Lab** s'han fet les següents passes:

## Pas 1: Accedir a AWS Management Console
* Authenticar-se a **AWS Academy Learner Lab**

* Accedir a:
 **AWS Academy Learner Lab > Modules > Learner Lab**
* Clicar a **Start Lab**
* Clicar a AWS (quan sigui verd)

## Pas 2: Accedir a AWS Cloud9 IDE
* Clicar a **Environments**
* Triar **Create environment**
Introduir el nom **ConsolaIDE**, triar una instància de feina gratuïta **t2.micro**, a **Network settings** triar **Secure Shell (SSH)** .
* Triar **Create**
* Obrir **ConsoleIDE** i accedir al bash.

## Pas 3: Definir el bucket de persistència
Abans de crear el clúster d'Saprk s'han de deinir els buckets a on volem fer la persistència de les dades.
Per això si no tenim cap bucket creat es poden executar les següents instruccions:
```
aws s3 mb s3://emr-cluster-40
```
Nota: Es pot donar el cas que el bucket ja estigui creat i ens doni un error. Es pot crear un altre amb un nom diferent.

## Pas 4: Crear l'estructura de carpetes del bucket

```
aws s3api put-object --bucket emr-cluster-40 --key config/
aws s3api put-object --bucket emr-cluster-40 --key jupyter/
aws s3api put-object --bucket emr-cluster-40 --key logs/
aws s3 ls s3://emr-cluster-40
```


## Pas 5: Descarregar l'script creació clúster
Crear el directori de treball i descarregar l'script de creació del clúster.
```
mkdir emrSparkJupyperhup
cd emrSparkJupyperhup
wget https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/emrSparkJupyterHub.sh
```

## Pas 6: Desplegar el clúster
Executar l'script. Afegir com a paràmetre el bucket s3 a on es farà la persistència
```
bash emrSparkJupyterHub.sh emr-cluster-40
```
## Pas 7: Permetre connexions SSH al clúster
Permetre la connexió ssh al clúster per això s'han de definir les regles al firewall.
1. Obrir la consola **Services EC2** 
1. Anar a **Instances** i seleccionar el máster.
1. Seleccionar la pestanya **Security** i a **Security groups** pitjar a la que fa refèrencia al màster.
1. Pijtar a **Edit inbound rules**
1. Pijtar a **Add rule**
1. Triar **SSH** i triar **Anywhere-ipv4 0.0.0.0/0**
1. Pijar **Save rules**

## Pas 8: Descarregar el certificat
A la pàgina de gestió de la consola AWS **Vocareum**
1. Seleccionar **AWS Details**
2. A SSH key pitjar a **Download PEM**
3. Posar permisos de només lectura al certificat.

## Pas 9: Configurar tunel SSH per a la connexió al jupyterhub
Dues opcions:
### Opcio 1:
Utilitzant instruccions ssh:
```
sudo ssh -i labsuser.pem -N -L localhost:9443:localhost:9443 ec2-user@ec2-3-239-255-75.compute-1.amazonaws.com
```

### Opció 2:
Utilitzant putty:
* A la secció de l'esquerra pitja a "Connection" i llavors a "SSH" per expandir les opcions.
* Fes clic a "Tunnels" 
* Afegexi el "Source port" i "Destination"

## Pas 10: Establir la connexió amb jupyterhub
Obrir el navegador amb la següent url:

https://localhost:9443

```
Username: jovyan
Password: jupyter
```
Seguidament es pot pujar el següent notebook de proves:
https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/M05_BDA_U2_P5_V08_Ejemplo%203-Clustering%20K-means.ipynb


## Prov
Altres tunels
http://ip-172-31-74-125.ec2.internal:20888/proxy/application_1714068935606_0008/

sudo ssh -i labsuser\ \(19\).pem -N -L 20888:ip-172-31-69-243.ec2.internal:20888 hadoop@ec2-44-222-179-40.compute-1.amazonaws.com

http://localhost:20888/proxy/application_1714068935606_0008/stages/


http://ip-172-31-69-83.ec2.internal:8042/

sudo ssh -i labsuser\ \(19\).pem -N -L 8042:ip-172-31-78-254.ec2.interna:8042 hadoop@ec2-44-222-179-40.compute-1.amazonaws.com

Altres referencies
https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-studio-magics.html

```
%pip install boto3 --upgrade
%pip install botocore --upgrade
%pip install emr-notebooks-magics --upgrade
```

provar de fer la instal·lacio de les dependènices a l'entorn ce noetobook
/emr/notebook-env/bin/pip install -U boto3
/emr/notebook-env/bin/pip install -U botocore
/emr/notebook-env/bin/pip install -U emr-notebooks-magics

