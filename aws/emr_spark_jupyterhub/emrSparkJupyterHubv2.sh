#!/bin/bash
# https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-jupyterhub.html
#mybucket=emr-cluster-40
mybucket=$1

# Preparar els fitxers de configuració
wget -qO- https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/install-my-jupyter-libraries.sh | aws s3 cp - "s3://$mybucket/config/install-my-jupyter-libraries.sh"
wget -qO- https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/datos_cluster.csv | aws s3 cp - "s3://$mybucket/jupyter/datos_cluster.csv"
wget https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/MyJupyterConfig.json

aws emr create-cluster \
 --name "emrSparkJupyterHub" \
 --log-uri "s3n://$mybucket/logs/" \
 --release-label emr-6.4.0 \
 --applications Name=Spark Name=JupyterHub \
 --instance-type m4.larg \
 --instance-count 3 \
 --use-default-roles \
 --ec2-attributes KeyName=vokey \
 --configurations 'file://MyJupyterConfig.json' \
 --instance-groups '[{"InstanceCount":1,"InstanceGroupType":"TASK","Name":"Task - 1","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":1,"InstanceGroupType":"CORE","Name":"Core","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":1,"InstanceGroupType":"MASTER","Name":"Primary","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}}]' \
 --bootstrap-actions "[{\"Args\":[],\"Name\":\"InstallJupyterLibs\",\"Path\":\"s3://$mybucket/config/install-my-jupyter-libraries.sh\"}]" \
 --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
 --region "us-east-1"

Aquest ha funcionat
aws emr create-cluster \
 --name "Development Cluster" \
 --release-label "emr-6.4.0" \
 --service-role "EMR_DefaultRole" \
 --ec2-attributes '{"InstanceProfile":"EMR_EC2_DefaultRole","EmrManagedMasterSecurityGroup":"sg-083264912af9faa81","EmrManagedSlaveSecurityGroup":"sg-0969c959a901d8096","KeyName":"vokey"}' \
 --applications Name=Spark Name=JupyterHub \
 --instance-groups '[{"InstanceCount":1,"InstanceGroupType":"MASTER","Name":"MASTER","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":2,"InstanceGroupType":"CORE","Name":"CORE","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}}]' \
 --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
 --region "us-east-1"

aws emr create-cluster \
 --name "My cluster" \
 --log-uri "s3n://emr-cluster-40/logs/" \
 --release-label "emr-7.1.0" \
 --service-role "arn:aws:iam::016062213481:role/EMR_DefaultRole" \
 --ec2-attributes '{"InstanceProfile":"EMR_EC2_DefaultRole","EmrManagedMasterSecurityGroup":"sg-083264912af9faa81","EmrManagedSlaveSecurityGroup":"sg-0969c959a901d8096","KeyName":"vockey","AdditionalMasterSecurityGroups":[],"AdditionalSlaveSecurityGroups":[],"SubnetId":"subnet-0386bc7f935985f4c"}' \
 --applications Name=Hadoop Name=Hive Name=JupyterEnterpriseGateway Name=JupyterHub Name=Livy Name=Spark \
 --instance-groups '[{"InstanceCount":1,"InstanceGroupType":"MASTER","Name":"Primary","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":1,"InstanceGroupType":"CORE","Name":"Core","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":1,"InstanceGroupType":"TASK","Name":"Task - 1","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}}]' \
 --bootstrap-actions '[{"Args":[],"Name":"jupyter","Path":"s3://emr-cluster-40/config/install-my-jupyter-libraries.sh "}]' \
 --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
 --region "us-east-1"

# aws emr create-cluster \
#  --name "emrSparkJupyterHub" \
#  --log-uri "s3n://$mybucket/logs/" \
#  --release-label "emr-5.36.1" \
#  --service-role "arn:aws:iam::010010875711:role/EMR_DefaultRole" \
#  --ec2-attributes '{"InstanceProfile":"EMR_EC2_DefaultRole","EmrManagedMasterSecurityGroup":"sg-03df8f4587717a10f","EmrManagedSlaveSecurityGroup":"sg-0cfd6b70cbd0a5594","KeyName":"vockey","AdditionalMasterSecurityGroups":[],"AdditionalSlaveSecurityGroups":[],"SubnetId":"subnet-0168af18428f16cc3"}' \
#  --applications Name=JupyterHub \
#  --configurations 'file://MyJupyterConfig.json' \
#  --instance-groups '[{"InstanceCount":1,"InstanceGroupType":"TASK","Name":"Task - 1","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":1,"InstanceGroupType":"CORE","Name":"Core","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}},{"InstanceCount":1,"InstanceGroupType":"MASTER","Name":"Primary","InstanceType":"m4.large","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp2","SizeInGB":32},"VolumesPerInstance":1}]}}]' \
#  --bootstrap-actions "[{\"Args\":[],\"Name\":\"InstallJupyterLibs\",\"Path\":\"s3://$mybucket/config/install-my-jupyter-libraries.sh\"}]" \
#  --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
#  --region "us-east-1"
