#!/bin/bash
# https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-jupyterhub.html
#mybucket=emr-cluster-40
mybucket="$1"

# Preparar els fitxers de configuració
wget -qO- https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/install-my-jupyter-libraries-v0.sh | aws s3 cp - "s3://$mybucket/config/install-my-jupyter-libraries-v0.sh"
wget -qO- https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/datos_cluster.csv | aws s3 cp - "s3://$mybucket/jupyter/datos_cluster.csv"
wget https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/aws/emr_spark_jupyterhub/MyJupyterConfig.json

echo $mybucket

sed -i "s/emr-cluster-40/$mybucket/g" MyJupyterConfig.json

aws emr create-cluster \
 --name \"emrSparkJupyterHub\" \
 --log-uri "s3n://$mybucket/logs/" \
 --release-label "emr-7.1.0" \
 --applications Name=Hadoop Name=Hive Name=JupyterEnterpriseGateway Name=JupyterHub Name=Livy Name=Spark \
 --instance-type m4.large \
 --instance-count 3 \
 --use-default-roles \
 --ec2-attributes KeyName=vockey \
 --region "us-east-1" \
 --configurations 'file://MyJupyterConfig.json' \
 --bootstrap-actions "[{\"Args\":[],\"Name\":\"InstallJupyterLibs\",\"Path\":\"s3://$mybucket/config/install-my-jupyter-libraries-v0.sh\"}]" \
 --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
 --auto-termination-policy '{"IdleTimeout":14400}' 
