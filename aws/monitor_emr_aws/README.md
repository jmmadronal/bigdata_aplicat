# Monitor EMR clúster
## Introducció 
En aquesta documentació trobareu les passes per monitorar un clúster EMR d'AWS. Per més informació seguiu el següent [enllaç](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-manage-view.html)

## Pas 0: Prerequisits
Per poder fer aquesta pràctica seria convenient tenir en funcionament un clúster d'EMR a AWS, per això es poden seguir les passes del següent document [EMR Spark](../aws/emr_spark_jupyterhub/README.md).

## Pas 1: Llançar el clúster EMR.
A **EMR on EC** seleccionar el clúster que volem utilitzar.
Si no hi cap clúster en funcionament, podem clonar un clúster que ens ha funcionat anteriorment i clonar-ho.
En tot cas ens hem d'assegurar que **Status** estigui en **Waiting**.
En tot cas a **sumary** apareix tota aquesta informació.

## Pas 2: Obrir les aplicacions UIs
Per obtenir informació sobre les mètriques del clúster per això pitja a:
* Spark History Server 
* YARN timeline server 
* Tez UI 


## Pas 2: Pujar programes pyspark
Es pot utilitzar qualsevol programa amb pyspark per utilitzar a la fase de proves. La idea és que siguin programes que la seva execució siguin alguns minuts.
Per tenir persistència dels programes s'aconsella pujar-los a la clúster.

Els programes proposats són:
[pi_calculation_spark.py](pyspark/pi_calculation_spark.py)
[words_count_files.py](pyspark/words_count_files.py)

Accedir al clúster EMR
```
ssh -i labsuser.pem  hadoop@ec2-3-236-9-159.compute-1.amazonaws.com
```

Crear una carpeta pyspark
```
mkdir pyspark
cd pyspark
```
Pujar el fitxer que es troben a la carpeta [pyspark](pyspark) a la carpeta que s'acaba de crear.
```
scp -i labsuser.pem ./bigdata_aplicat/aws/monitor_emr_aws/pyspark/* hadoop@ec2-3-236-9-159.compute-1.amazonaws.com:~/pyspark
```

Pujar els fitxers de text al hdfs
```
hdfs dfs -put ContesDAndersen.txt
hdfs dfs -put LaucadelsenyorEsteve.txt
hdfs dfs -put ElTresordelVellCavaller.txt
hdfs dfs -put LesAventuresDeTomSawyer.txt
hdfs dfs -put ElsHerois.txt
hdfs dfs -put Locatalanisme.txt
hdfs dfs -put Lafestadelsreis.txt
hdfs dfs -put tirantloblanc.txt 
hdfs dfs -put LapotecarideMalgrat.txt
```


## Pas 3: Començar a utilitzar el clúster amb spark:
L'objectiu d'aquesta passa és poder llançar programes fets amb pyspark o els notebooks que podem executar amb el jupyterlab. Veure els diferents modes de fer-ho i veure com es van repartint les tasques als diferents nodes del clúster. 
Un altre objectiu és poder monitorar els recursos que s'estan utilitzant als diferents nodes del clúster.
Per això s'utilitzaran les mètriques vistes a la passa anterior.

### Llançar un programa pyspark amb spark-submit
Spark-submit permet llançar qualsevol mena de programa spark entre ells pyspark, per conèixer totes les possibilitats del programa podem accedir a l'ajuda.
```
spark-submit --help
```
Exemple dels paràmetres que s'utilitzaran:
```
spark-submit \
    --master <URL_del_mestre_del_clúster> \
    --executor-memory <memòria_per_executor> \
    --num-executors <nombre_d'executors> \
    --executor-cores <nombre_de_cores> \
    ruta/al/teu/arxiu.jar
```
#### Llançar un programa pyspark amb spark-submit en local
Aquesta comanda s'ha d'executar des de la consola del master.
```
spark-submit --master local words_count_files.py
```
Quan es llança en local només s'utilitza l'executor del mestre i no s'utilitzen els altres nodes del clúster.

Es pot llançar aprofitan el màxim de cores del node mestre:
``` 
spark-submit --master local[*] pi_calculation_spark.py
```

#### Llançar un programa pyspark amb spark-submit en el clúster
Aquesta comanda s'ha d'executar des de la consola del màster.
```
spark-submit --master yarn --num-executors 3 pi_calculation_spark.py
```
El que fem és utilitzar el gestor de recursos del clúster **YARN** per gestionar les tasques que va llançant l'spark

Es pot llançar aprofitan el màxim de cores del node mestre:
```
spark-submit --master yarn --num-executors 3 --executor-cores 2 pi_calculation_spark.py
```
Per cada sessió spark s'arranca un SparkUI a on es pot comprovar l'estat de la sessió. Generalment quan arranca el procés s'indica a quin port s'estableix la connexió.

## Referències
View and monitor a cluster. AWS.s
https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-manage-view.
html

