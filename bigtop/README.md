# BigTop - Cluster Hadoop - proxmox 
## Introducció 
BigTop és un projecte per al desenvolupament de paquets i tests de l'ecosistema Apache Hadoop.

En aquests documents s'explicarà com implementar un desplegament d'un clúster Hadoop amb BitTop a un contenidor LXC amb docker a proxmox.

## Pas 1: Creació del contenidor LXC
* Accedir al clúster de promox [Servei Proxmox](https://docs.google.com/presentation/d/e/2PACX-1vSXM3IWHYWIyiND6zufMU8xKTSzV8UhS_DspbVzqFpGheC-J5ChMS5wh4kgC-d40pecoTDIvxNZtmAC/pub?start=false&loop=false&delayms=3000)
* Clicar a **Create CT**

A la pipella **General**
* Triar el node (*)
* Escriure el nom del contenidor a **Hostname**
* Desmarcar el check box **Unprivileged container:**
* Triar **Resource Pool**
* Escriure una contrasenya i confirmar-la.
* Pitjar **Next**

A la pipella **Template**
* A **Storage** triar ISOs
* A **Template** triar ubuntu-22.04-standard
* Pitjar **Next**

A la pipella **Disks**
* A **Storage** triar local-lvm
* A **Disk size** posar 100
* Pitjar **Next**

A la pipella **CPU**
* A **Cores** posar 4
* Pitjar **Next**
A la pipella **Memory**
* A **Memory (MiB)** posar 8192
* A **Swap (MiB)** posar 8192
* Pitjar **Next**
A la pipella Network
* A **IPv4** triar **DHCP**
* Pijtar **Finish**

Una vegada creat el contenidor LXC confirmar a **Options** que:
* Unprivileged container = N0
* Features nesting=1

Arrancar el contenidor pitjar **Start**
Entrar a **Console** (**)

> (*) Els clústers estan formats per diversos hipervisors amb diferents capacitats, crear el contenidor al node que s'ajusti millor a les necessitats del contenidor.

> (**) Es pot entrar amb la consola amb ssh en aquest cas el que s'ha de fer és el fitxer "/etc/ssh/sshd_config" posar la directiva "PermitRootLogin yes" després s'ha de reiniciar el servei `systemctl restart sshd` 

## Pas 2: Instal·lació dels prerequisits
Per poder fer el provisionalment de bigtop amb docker amb el contenidor LXC s'han d'instal·lar: git, ruby, docker, docker-compose

* Actualització del sistema:
  ```
  apt update
  apt upgrade
  ```
  https://docs.docker.com/engine/install/ubuntu/
* Afegir el respositoris oficials de docker:
  ```
  apt update
  # Add Docker's official GPG key:
  sudo apt-get update
  sudo apt-get install ca-certificates curl
  sudo install -m 0755 -d /etc/apt/keyrings
  sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
  sudo chmod a+r /etc/apt/keyrings/docker.asc
  # Add the repository to Apt sources:
  echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get update
  ```
* Instal.lació paquets docker:
  ```
  sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-compose
   ```
* Arrancar el dimoni de Docker
  ```
  service docker start
  ```
* Instal.lació git:
  ```
  apt install git
  ```
* Instal.lació ruby:
  ```
  apt install ruby
  ```

## Pas 3: Descarregar el repositori de bigtop
En aquest cas s'ha decidit descarregar la versió 3.1.1, ja que té alguns paquets addicionals.
Descarregar:
```
wget https://archive.apache.org/dist/bigtop/bigtop-3.1.1/bigtop-3.1.1-project.tar.gz
```
Descomprimir i accedir al directori:
```
tar -xf bigtop-3.1.1-project.tar.gz
cd bigtop-3.1.1/
```

## Pas 4: Preparar els components del clúster:
En aquest pas s'ha de decidir quins components es volen 
aprovisionar al clúster. 

Accedir al directori d'aprovisionament:
```
cd provisioner/docker/
```
Adoptam un config.yaml:
```
cp config_ubuntu-20.04.yaml config.yaml
```
Editam el config.yaml i comprovam que tingui els components:
```
components: [hdfs,yarn,mapreduce,hive,sqoop]
```
Editam el fitxer docker-compose-cgroupv2.yml perquè es publiquin els ports de hdfs, yarn i hive des d'una xarxa externa.
```
    ports:
# HDFS NameNode Web UI
    - 57000-57099:50070
# HDFS DataNode Web UI
    - 56400-56499:9864
# YARN ResourceManager Web UI
    - 18800-18899:8088
# YARN NodeManager Web UI
    - 14200-14299:8042
# Hive
    - 10200-10299:10002
```

## Pas 5: Llançar el clúster hadoop:
Llançar el clúster hadoop amb tres nodes.
```
./docker-hadoop.sh -F docker-compose-cgroupv2.yml --create 3
```
Comprovar que el cluster està operatiu i quin son els ports assiganats a cada node
```
./docker-hadoop.sh -l
```

## Pas 6: Començar a utilitzar el clúster:
Accedir al node mestre del cluster:
```
./docker-hadoop.sh --exec 1 bash
```

## Referències
https://github.com/apache/bigtop/

https://bigtop.apache.org/

https://docs.docker.com/engine/install/ubuntu/



