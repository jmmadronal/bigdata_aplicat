# Especificamos las versiones que se utilizarán de Spark, Hadoop y JupyterLab
SPARK_VERSION="3.1.2"
#HADOOP_VERSION="3.2"
JUPYTERLAB_VERSION="3.2.0"

# Creación de la imagen de Docker jupyterlab
docker build \
  --build-arg spark_version="${SPARK_VERSION}" \
  --build-arg jupyterlab_version="${JUPYTERLAB_VERSION}" \
  -f jupyterlab.Dockerfile \
  -t jupyterlab .
