# Spark Metrics amb Apache BigTop  
## Introducció 
En aquesta documentació trobareu les passes per monitorar un clúster d'Spark utilitzant les mateixes eines que a l'Apache Spark s'ha desenvolupat.

### Importància del Monitoratge en Spark
**Per què monitorar?**
* Garantir l’eficiència i el rendiment
* Detectar i resoldre colls de botellaa
* Optimitzar recursos
* Assegurar-se que les aplicacions compleixin amb els SLA (Service Level Agreements)

### Components Clau de Spark Metrics
#### Executors
* Recursos de treball que realitzen el processament de dades
* Mètriques importants:
  * Utilització de la memòria
  * Temps de CPU
  * E/S del dis

#### Tasks
* Unitats de treball executades pels executors
* Mètriques importants:
  * Temps d'execució
  * Temps d'esper
  * Bytes llegits i escrits

#### Jobs
* Conjunt de tasques per completar una operació específica
* Mètriques importants:
  * Temps total de feina
  * Tasques fallides o reeixides

#### Stages
* Subdivisions dels jobs
* Mètriques importants:
  * Temps d'execució de cada etapa
  * Progressió de les tasques

Totes aquestes mètriques es poden visualitzar amb la Interfície Web que el propi Apache Spark ha desenvolupat. Però també es poden integrar amb altres sistemes de monitoratge com: Prometheus, Grafana ...

A les passes següents és mostra com accedir a la Interfície Web de Spark, per això es crearà un clúster amb bigtop com s'ha fet a la part de [bigtop](../bigtop/README.md)

## Pas 0: Prerequisits
Abans de tot s'ha d'haver fet les passes per poder crear un clúster amb [bigtop](../bigtop/README.md) passes 1 a 3.

## Pas 1: Preparar els components del clúster:
En aquestes passes es crearà un clúster amb els següents components: 
hdfs, yarn, mapreduce, hive, sqoop, spark, livy

Accedir al directori d'aprovisionament:
```
cd provisioner/docker/
```
Adoptam el fitxer config.yaml, aquest és un exemple:
https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/spark_metrics_bigtop/config.yaml

```
wget https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/spark_metrics_bigtop/config.yaml

```
Editam el fitxer docker-compose-cgroupv2.yml perquè es publiquin els ports de hdfs, yarn, hive, spark, lyvi des d'una xarxa externa, aquest és un exemple:
https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/spark_metrics_bigtop/docker-compose-cgroupv2.yml?ref_type=heads
```
wget https://gitlab.com/jmmadronal/bigdata_aplicat/-/raw/main/spark_metrics_bigtop/docker-compose-cgroupv2.yml?ref_type=heads
```

## Pas 5: Llançar el clúster hadoop:
Llançar el clúster hadoop amb tres nodes.
```
./docker-hadoop.sh -F docker-compose-cgroupv2.yml --create 3
```
Comprovar que el cluster està operatiu i quin son els ports assiganats a cada node
```
./docker-hadoop.sh -l
```

## Pas 6: Comprovar com s'han mapajats els ports i es poden accedir a les mètriques.
Per fer això el més còmode és obrir dues consoles amb SSH.

Accedir al node mestre del cluster:
```
./docker-hadoop.sh --exec 1 bash
```
Comprovar com s'han mapajats els ports:
```
docker ps
```
Accedir a les diferents mètriques amb el navegador.
Per exemple:
* Accedir al Hadoop NameNode.

  URL original: http://hostname:50070

  URL clúster docker: http://ipdockeranfitrio:5700?

  URL per visualitzar totes les mètriques: http://hostname:50070/jmx

  URP per visualtizar la memòria: http://hostname:50070/jmx?qry=java.lang:type=Memory

* Accedr al Hadoop DataNode

  URL original: http://hostname:9864 (a bigtop)

  URL original: http://hostname:50075 (genèrica)

  URL clúster docker: http://ipdockeranfitrio:5640?
  
  URL per visualitzar totes les mètriques: http://hostname:50075/jmx

  URP per visualitzar la memòria: http://hostname:50075/jmx?qry=java.lang:type=Memory

* Accedir al gestor de recursos del clúster **YARN**
  
  URL original: http://hostname:8088

  URL clúster docker: http://ipdockeranfitrio:1880?

  URL per visualitzar totes les mètriques: http://hostname:8088/jmx

  URL per visualitzar la memòria: http://hostname:8088/jmx?qry=java.lang:type=Memory

* Accedir als gestors de recursos dels nodes clúster **YARN**
  
  URL original: http://hostname:8042

  URL clúster docker: http://ipdockeranfitrio:1420?

  URL per visualitzar totes les mètriques: http://hostname:8042/jmx

  URL per visualitzar la memòria: http://hostname:8042/jmx?qry=java.lang:type=Memory

## Pas 7: Pujar programes pyspark
Es pot utilitzar qualsevol programa amb pyspark per utilitzar a la fase de proves. La idea és que siguin programes que la seva execució siguin alguns minuts.
Per tenir persistència dels programes s'aconsella pujar-los a la carpeta bigtop de l'amfitrió del clúster.

Els programes proposats són:
[pi_calculation_spark.py](pyspark/pi_calculation_spark.py)
[words_count_files.py](pyspark/words_count_files.py)

Crear una carpeta pyspark
```
cd bigtop-3.1.1/
mkdir pyspark
cd pyspark
```
Pujar el fitxer que es troben a la carpeta [pyspark](pyspark) que s'acaba de crear.

Accedir al node mestre del clúster. 
```
./docker-hadoop.sh --exec 1 bash
```
Accedir al directori /bigtop-home/pyspark:
```
cd /bigtop-home/pyspark/
```

Pujar els fitxers de text al hdfs
```
hdfs dfs -put ContesDAndersen.txt
hdfs dfs -put LaucadelsenyorEsteve.txt
hdfs dfs -put ElTresordelVellCavaller.txt
hdfs dfs -put LesAventuresDeTomSawyer.txt
hdfs dfs -put ElsHerois.txt
hdfs dfs -put Locatalanisme.txt
hdfs dfs -put Lafestadelsreis.txt
hdfs dfs -put tirantloblanc.txt 
hdfs dfs -put LapotecarideMalgrat.txt
```

## Pas 8: Començar a utilitzar el clúster amb spark:
L'objectiu d'aquesta passa és poder llançar programes fets amb pyspark  o els propis "smoke tests" que implementa Apache Bigtop  . Veure els diferents modes de fer-ho i veure com es van repartint les tasques als diferents nodes del clúster. 
Un altre objectiu és poder monitorar els recursos que s'estan utilitzant als diferents nodes del clúster.
Per això s'utilitzaran les mètriques vistes a la passa anterior.

### Llançar un programa pyspark amb spark-submit
Spark-submit permet llançar qualsevol mena de programa spark entre ells pyspark, per conèixer totes les possibilitats del programa podem accedir a l'ajuda.
```
spark-submit --help
```
Exemple dels paràmetres que s'utilitzaran:
```
spark-submit \
    --master <URL_del_mestre_del_clúster> \
    --executor-memory <memòria_per_executor> \
    --num-executors <nombre_d'executors> \
    --executor-cores <nombre_de_cores> \
    ruta/al/teu/arxiu.jar
```
#### Llançar un programa pyspark amb spark-submit en local
Aquesta comanda s'ha d'executar des de la consola del master.
```
spark-submit --master local count_words_files.py
```
Quan es llança en local només s'utilitza l'executor del mestre i no s'utilitzen els altres nodes del clúster.

Es pot llançar aprofitan el màxim de cores del node mestre:
``` 
spark-submit --master local[*] pi_calculation_spark.py
```
> Nota: Aquest algorisme es pot torbar molt depenent del nombre de punts generats

Per cada sessió spark s'arranca un SparkUI a on es pot comprovar l'estat de la sessió. Generalment quan arranca el procés s'indica a quin port s'estableix la connexió.

* URL original: http://hostname:4040 (Si hi ha més d'una sessió seran correlatives)

* URL clúster docker: http://ipdockeranfitrio:4400?

#### Llançar un programa pyspark amb spark-submit en el clúster
Aquesta comanda s'ha d'executar des de la consola del màster.
```
spark-submit --master yarn --num-executors 3 pi_calculation_spark.py
```
El que fem és utilitzar el gestor de recursos del clúster **YARN** per gestionar les tasques que va llançant l'spark

Es pot llançar aprofitan el màxim de cores del node mestre:
```
spark-submit --master yarn --num-executors 3 --executor-cores 2 pi_calculation_spark.py
```
Per cada sessió spark s'arranca un SparkUI a on es pot comprovar l'estat de la sessió. Generalment quan arranca el procés s'indica a quin port s'estableix la connexió.
En aquest cas podem accedir al gestor de recursos del clúster **YARN**

*  URL original: http://hostname:8088

*  URL clúster docker: http://ipdockeranfitrio:1880?

Amb aquest gestor podem veure tots els processos que s'estan executant en aquest moment en el clúster. Per accedir a l'SparkUI el que s'ha de fer és accedir amb el proxy que llança **YARN** per monitorar les sessions d'spark.

*  URL original: http://hostname:20888/proxy/application_1715880597196_0034
*  URL clúster docker: http://ipdockeranfitrio:2080?proxy/application_1715880597196_0034

## Referències
Monitoring and Instrumentation. Apache Spark.
https://spark.apache.org/docs/latest/monitoring.html#web-interfaces

Web UI. Apache Spark.
https://spark.apache.org/docs/latest/web-ui.html



