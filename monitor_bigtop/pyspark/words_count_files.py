from pyspark.sql import SparkSession

if __name__ == "__main__":
    # Création d'une session Spark
    spark = SparkSession.builder \
        .appName("WordCount") \
        .getOrCreate()

    llibres = ["tirantloblanc.txt", "ContesDAndersen.txt", "Locatalanisme.txt", "ElsHerois.txt", 
        "ElTresordelVellCavaller.txt", "Lafestadelsreis.txt", "LapotecarideMalgrat.txt",
        "LaucadelsenyorEsteve.txt", "LesAventuresDeTomSawyer.txt" ]
    for i in llibres:
    # Chargement du fichier texte
        lines = spark.read.text(i).rdd.map(lambda r: r[0])

    # Split des lignes en mots
        words = lines.flatMap(lambda line: line.split(" "))

    # Mapping des mots à des paires (mot, 1) pour comptage
    word_counts = words.map(lambda word: (word, 1)).reduceByKey(lambda x, y: x + y)

    # Convertir l'RDD en DataFrame
    word_counts_df = spark.createDataFrame(word_counts, ["word", "count"])

    sorted_word_counts = word_counts_df.orderBy("count", ascending=False)

    # Sauvegarde des résultats dans un fichier texte
    sorted_word_counts.write.csv("word-count-v1.csv")

    # Arrêt de la session Spark
    spark.stop()
