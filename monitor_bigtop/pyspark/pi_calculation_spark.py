
from pyspark.sql import SparkSession
import random

def is_point_inside_unit_circle(_):
    x, y = random.random(), random.random()
    return 1 if x**2 + y**2 <= 1 else 0

if __name__ == "__main__":
    # Creacio d'una sessio Spark
    spark = SparkSession.builder \
        .appName("Pi Calculation") \
        .getOrCreate()

    # Nombre de punts a gener
    # 36mins, 39sec
    # num_samples = 10000000000
    num_samples = 1000000000

    # Creació d'un RDD amb el nombre de punts especifcat
    rdd = spark.sparkContext.parallelize(range(num_samples))

    # Aplicacio de la funcio per determinar si el punt esta dins el cercle
    points_inside_circle = rdd.map(is_point_inside_unit_circle).reduce(lambda a, b: a + b)

    # Calcul de pi
    pi_estimate = 4.0 * points_inside_circle / num_samples
    pi_estimate.saveAsTextFile("pi-10e8-samples")
    print(f"Estimation de Pi est : {pi_estimate}")


    # Aturar la sessió d'Spark
    spark.stop()
